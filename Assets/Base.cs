using System;
using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;
using UnityEngine.Serialization;

public class Base : MonoBehaviour
{
    public GameObject warriorPrefab;
    
    public IEnumerator InstantiateWarriors(PathCreator pathCreator)
    {
        for (int i = 0; i < 5; i++)
        {
            
            var warrior = Instantiate(warriorPrefab, pathCreator.bezierPath.GetPoint(0), Quaternion.identity, pathCreator.transform);
            Warrior warriorComponent = warrior.GetComponent<Warrior>();
            warriorComponent.Move(pathCreator);
            warriorComponent.warriorDie += OnWarriorDie;
            yield return new WaitForSeconds(.5f);
        }

        PathController pathController = pathCreator.gameObject.GetComponent<PathController>();
        Debug.Log("Go to pathController");
        StartCoroutine(pathController.DestroyPath(pathCreator));
    }

    private void OnWarriorDie()
    {
        Debug.Log("Warrior die");
    }

    private void Awake()
    {
        
    }
}
