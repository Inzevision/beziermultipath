﻿using System.Collections.Generic;
using PathCreation;
using UnityEngine;


    using Unity;
    public class ConcretePath
    {
        private List<Transform> _points;

        public ConcretePath(List<Transform> points)
        {
            _points = points;
        }

        public BezierPath Create()
        {
            return new BezierPath(_points, false, PathSpace.xy);
        }
    }
