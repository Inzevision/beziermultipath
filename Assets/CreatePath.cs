using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using PathCreation;
using UnityEngine.EventSystems;

public class CreatePath : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public Transform otherBase;
    public PathCreator pathCreator;
    
    private BezierPath _bezierPath;
    private Base _base;
    private LineRenderer _lr;
    private VertexPath _vp;
    private Vector3 _bezierMiddlePoint;
    private PathCreator _pathCreator;

    public BezierPath BezierPath
    {
        set { _bezierPath = value; }
        get { return _bezierPath; }
    }

    void Start()
    {
        _lr = GetComponent<LineRenderer>();
        _base = GetComponent<Base>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        _bezierMiddlePoint = Camera.main.ScreenToWorldPoint(eventData.position);
        _bezierMiddlePoint.z = 0;
        _bezierPath.SetPoint(2, _bezierMiddlePoint, false);
        _vp = new VertexPath(_bezierPath, transform, 0.1f);
        _lr.positionCount = _vp.localPoints.Length;
        _lr.SetPositions(_vp.localPoints);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        CreateBasePath();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        StartCoroutine(_base.InstantiateWarriors(_pathCreator));
    }

    private void CreateBasePath()
    {
        List<Transform> points = new List<Transform>(){transform, otherBase.transform};
        _pathCreator = Instantiate(pathCreator);
        if (points.Count > 0)
        {
            _bezierPath = new BezierPath(points, false, PathSpace.xy);
            _pathCreator.bezierPath = _bezierPath;
        }
        //Debug.Log($"Anchor points: {bezierPath.NumAnchorPoints}. Total points {bezierPath.NumPoints}");
        _bezierPath.SetPoint(1, _bezierPath.GetPoint(0), false);
    }
}
