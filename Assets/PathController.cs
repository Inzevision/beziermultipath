using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using PathCreation;
using UnityEngine;

public class PathController : MonoBehaviour
{
    // Start is called before the first frame update
    private int _warriorCount, _currentWarriorCount;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_warriorCount > 0)
        {
            _currentWarriorCount = transform.GetComponentsInChildren<Warrior>().Length;
        }
        //Debug.Log($"Warriors at path {_warriorCount}, Current count {_currentWarriorCount}");
    }


    public IEnumerator DestroyPath(PathCreator pathCreator)
    {
        _warriorCount = _currentWarriorCount = pathCreator.transform.GetComponentsInChildren<Warrior>().Length;
        yield return new WaitWhile(() => _currentWarriorCount > 0);
        Destroy(gameObject);
    }
}
