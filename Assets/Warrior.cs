using System;
using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;
using UnityEngine.EventSystems;

public class Warrior : MonoBehaviour
{
    public float pathPosition;
    private PathCreator _pathCreator;
    public event Action warriorDie;

    public void Move(PathCreator pathCreator)
    {
        _pathCreator = pathCreator;
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        if (transform != null)
        {
            pathPosition += 2f * Time.deltaTime;
            transform.position = _pathCreator.path.GetPointAtDistance(pathPosition, EndOfPathInstruction.Stop);

            if (transform.position == FindObjectOfType<CreatePath>().otherBase.position)
            {
                WarriorSelfDestroy();
            }
        }
    }

    private void WarriorSelfDestroy()
    {
        warriorDie?.Invoke();
        Destroy(this.gameObject);
    }
}
